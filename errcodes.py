class ErrCodes:
	OK 						= 1
	VALID_FULL				= 2
	VALID_NONFULL			= 3
	FAIL_SIZE 				= -1
	FAIL_INVALID_INITIALS	= -2
	FAIL_UNINITED			= -3
	FAULTY					= -4
	FAIL_INVALID_INPUT		= -5
	FAIL_INITIAL			= -6
	FAIL_OUTOFBOUNDS		= -7
	FAIL_EMPTY				= -8
	FAIL_INVALIDARGUMENT	= -9

	message ={														\
	1:"OK",															\
	2:"Je OK, polna mreza :D",										\
	3:"Je OK, mreza se ni polna",									\
	-1:"Napaka v velikost (nepodprto, ?)",							\
	-2:"Napaka v zacetnih vrednostih",								\
	-3:"Napaka, mreza ni inicalizirana",							\
	-4:"Mreza NI OK, napacne vrednosti nekje ><",					\
	-5:"Vhodna vrednost ni v mejah",								\
	-6:"Na tem mestu je zacetna vrednost, ne gre spreminjat!",		\
	-7:"Lokacija je neveljavna",									\
	-8:"Lokacija je prazna",											\
	-9:"Napaka v vhodnem argumentu"									\
	}
	
	def getMsg(self, koda):
		if koda in self.message:
			return self.message[koda]
		else:
			return "Neznana koda napake :("
		
kode = ErrCodes()	# Ze zdefinirano, ni pa treba.
