import unittest
import random
from errcodes import ErrCodes as ec
from acmesudoku import Sudoku	# Razred

import numpy
import HtmlTestRunner

class TestInit(unittest.TestCase):
	
	def test_kreiranje3x3(self):
		with self.assertRaises(Exception) as inst:
			Sudoku(3, 3)

		num, msg = inst.exception.args
		
		self.assertTrue(ec().FAIL_SIZE == num)
		self.assertTrue(ec().getMsg(ec().FAIL_SIZE) == msg)

	def test_kreiranje9x9(self):
		Sudoku(9, 9)
		self.assertTrue(True)

	def test_kreiranje16x16(self):
		Sudoku(16, 16)
		self.assertTrue(True)

	def test_kreiranjeRandom(self):
		random.seed()
		for x in range(0, 100):
			tempX = random.randint(-100, 100)
			tempY = random.randint(-100, 100)
			if (tempX == tempY):
				tempY = tempY+1
			
			with self.assertRaises(Exception) as inst:
				Sudoku(tempX, tempY)

			num, msg = inst.exception.args
			
			self.assertTrue(ec().FAIL_SIZE == num)
			self.assertTrue(ec().getMsg(ec().FAIL_SIZE) == msg)
			
	
	def test_kreiranje9x9ZacetnaDefiniranaOK1(self):
		temp = Sudoku(9, 9, vrednosti = [[0]*9,[0]*9,[0]*9,[0]*9,[0]*9,[0]*9,[0]*9,[0]*9,[0]*9])

	def test_kreiranje9x9ZacetnaDefiniranaOK2(self):
		random.seed()
		tempMreza = [[0]*9,[0]*9,[0]*9,[0]*9,[0]*9,[0]*9,[0]*9,[0]*9,[0]*9]
		for vrednost in range(1, 9+1):
			tempX = random.randint(0, 8)
			tempY = random.randint(0, 8)
			while (tempX == tempY or tempMreza[tempX][tempY] != 0):
				tempX = random.randint(0, 8)
				tempY = random.randint(0, 8)
			tempMreza[tempX][tempY] = vrednost
		temp = Sudoku(9, 9, vrednosti = tempMreza)
		
	def test_kreiranje9x9ZacetnaDefiniranaOK3(self):
		temp = Sudoku(9, 9, nakljucneVrednosti = 0)

	def test_kreiranje9x9ZacetnaDefiniranaOK4(self):
		temp = Sudoku(9, 9, nakljucneVrednosti = 9)

	def test_kreiranje9x9ZacetnaDefiniranaOK5(self):
		temp = Sudoku(9, 9, nakljucneVrednosti = 81)
		

	def test_kreiranje9x9ZacetnaDefiniranaNOK1(self):
		random.seed()
		tNum = random.randint(1, 16)

		with self.assertRaises(Exception) as inst:
			Sudoku(9, 9, vrednosti = [[tNum]*9]*9)

		num, msg = inst.exception.args
		
		self.assertTrue(ec().FAIL_INVALID_INITIALS == num)
		self.assertTrue(ec().getMsg(ec().FAIL_INVALID_INITIALS) == msg)

	
	def test_kreiranje9x9ZacetnaDefiniranaNOK2(self):
		with self.assertRaises(Exception) as inst:
			Sudoku(9, 9, vrednosti = [[0]*10]*10)

		num, msg = inst.exception.args
		
		self.assertTrue(ec().FAIL_INVALID_INITIALS == num)
		self.assertTrue(ec().getMsg(ec().FAIL_INVALID_INITIALS) == msg)
	
	def test_kreiranje9x9ZacetnaDefiniranaNOK3(self):
		with self.assertRaises(Exception) as inst:
			Sudoku(9, 9, vrednosti = [[0]*8]*8)

		num, msg = inst.exception.args
		
		self.assertTrue(ec().FAIL_INVALID_INITIALS == num)
		self.assertTrue(ec().getMsg(ec().FAIL_INVALID_INITIALS) == msg)
	
	#def test_kreiranje9x9ZacetnaDefiniranaNOK4(self):
	#	with self.assertRaises(Exception) as inst:
	#		Sudoku(9, 9, nakljucneVrednosti = -9)

	#	num, msg = inst.exception.args
		
	#	self.assertTrue(ec().FAIL_INVALID_INITIALS == num)
	#	self.assertTrue(ec().getMsg(ec().FAIL_INVALID_INITIALS) == msg)

	def test_kreiranje9x9ZacetnaDefiniranaNOK5(self):
		with self.assertRaises(Exception) as inst:
			Sudoku(9, 9, nakljucneVrednosti = 100)

		num, msg = inst.exception.args
		
		self.assertTrue(ec().FAIL_INVALID_INITIALS == num)
		self.assertTrue(ec().getMsg(ec().FAIL_INVALID_INITIALS) == msg)

	def test_kreiranje9x9ZacetnaDefiniranaNOK6(self):
		with self.assertRaises(Exception) as inst:
			Sudoku(9, 9, vrednosti = [[0]*9]*9, nakljucneVrednosti = 3)

		num, msg = inst.exception.args
		
		self.assertTrue(ec().FAIL_INVALID_INITIALS == num)
		self.assertTrue(ec().getMsg(ec().FAIL_INVALID_INITIALS) == msg)


class TestDobiMrezo(unittest.TestCase):
	
	def test_dobiMrezo9x9(self):
		temp = Sudoku(9, 9)
		errkoda, tMreza, velX, velY = temp.dobiMrezo()
		self.assertTrue(ec().OK == errkoda)
		self.assertTrue(velX == velY and velY == 9 and len(tMreza) == 9 and len(tMreza[8]) == 9)

	def test_dobiMrezoNeodvisnost(self):
		temp = Sudoku(9, 9)
		errkoda, tMreza, velX, velY = temp.dobiMrezo()
		tMreza[3][4]=100
		errkoda, tMrezaDva, velX, velY = temp.dobiMrezo()
		self.assertTrue(ec().OK == errkoda)
		self.assertFalse(numpy.array_equal(tMreza, tMrezaDva))

class TestPreveriMrezo(unittest.TestCase):
	
	def test_PreveriMrezo1(self):
		temp = Sudoku(9, 9)
		errKoda, seznam = temp.preveriMrezo()
		self.assertTrue(ec().VALID_NONFULL == errKoda)
	
	def test_PreveriMrezo2(self):
		temp = Sudoku(9, 9, vrednosti = \
							[	[v for v in range(1, 9+1)], \
								[v for v in range(4, 9+1)] + [v for v in range(1, 3+1)],\
								[v for v in range(7, 9+1)] + [v for v in range(1, 6+1)],\
								[v for v in range(2, 9+1)] + [v for v in range(1, 1+1)],\
								[v for v in range(5, 9+1)] + [v for v in range(1, 4+1)],\
								[v for v in range(8, 9+1)] + [v for v in range(1, 7+1)],\
								[v for v in range(3, 9+1)] + [v for v in range(1, 2+1)],\
								[v for v in range(6, 9+1)] + [v for v in range(1, 5+1)],\
								[v for v in range(9, 9+1)] + [v for v in range(1, 8+1)]])

		errKoda, seznam = temp.preveriMrezo()
		self.assertTrue(ec().VALID_FULL == errKoda)

	def test_PreveriMrezo3(self):
		temp = Sudoku(9, 9)
		temp.vpisiVrednost(0, 0, 1)
		temp.vpisiVrednost(0, 8, 1)
		temp.vpisiVrednost(3, 0, 1)
		temp.vpisiVrednost(4, 4, 1)
		errKoda, seznam = temp.preveriMrezo()
		self.assertTrue(ec().FAULTY == errKoda and len(seznam) == 3)
		self.assertTrue([0,0] in seznam and [0, 8] in seznam and [3, 0] in seznam)


class TestVpisiVrednost(unittest.TestCase):
	def test_VpisiVrednost1(self):
		temp = Sudoku(9, 9)
		self.assertTrue(ec().OK == temp.vpisiVrednost(0, 1, 1))
		self.assertTrue(ec().OK == temp.vpisiVrednost(3, 8, 3))
		self.assertTrue(ec().OK == temp.vpisiVrednost(7, 6, 9))
		errkoda, tMreza, velX, velY = temp.dobiMrezo()
		self.assertTrue(tMreza[0][1] == 1 and tMreza[3][8] == 3 and tMreza[7][6] == 9)

	def test_VpisiVrednost2(self):
		temp = Sudoku(9, 9)
		self.assertTrue(ec().FAIL_INVALID_INPUT == temp.vpisiVrednost(0, 1, 0))
		self.assertTrue(ec().FAIL_INVALID_INPUT == temp.vpisiVrednost(0, 1, -1))
		self.assertTrue(ec().FAIL_INVALID_INPUT == temp.vpisiVrednost(0, 1, 10))

	def test_VpisiVrednost3(self):
		temp = Sudoku(9, 9, 	[[v for v in range(1, 9+1)], \
								[v for v in range(4, 9+1)] + [v for v in range(1, 3+1)],\
								[v for v in range(7, 9+1)] + [v for v in range(1, 6+1)],\
								[0]*9,[0]*9,[0]*9,[0]*9,[0]*9,[0]*9])

		self.assertTrue(ec().FAIL_INITIAL == temp.vpisiVrednost(0, 0, 1))
		self.assertTrue(ec().FAIL_INITIAL == temp.vpisiVrednost(2, 8, 1))
		self.assertTrue(ec().FAIL_INITIAL == temp.vpisiVrednost(1, 4, 1))

	def test_VpisiVrednost4(self):
		temp = Sudoku(9, 9)
		self.assertTrue(ec().FAIL_OUTOFBOUNDS == temp.vpisiVrednost(-1, 0, 1))
		self.assertTrue(ec().FAIL_OUTOFBOUNDS == temp.vpisiVrednost(5, 10, 1))
		self.assertTrue(ec().FAIL_OUTOFBOUNDS == temp.vpisiVrednost(9, 9, 1))

class TestZbrisiVrednost(unittest.TestCase):
	def test_izbrisiVrednost1(self):
		temp = Sudoku(9, 9)
		self.assertTrue(ec().OK == temp.izbrisiVrednost(0, 0))
		self.assertTrue(ec().OK == temp.izbrisiVrednost(2, 7))
		self.assertTrue(ec().OK == temp.izbrisiVrednost(8, 8))

	def test_izbrisiVrednost2(self):
		temp = Sudoku(9, 9)
		temp.vpisiVrednost(2, 2, 1)
		self.assertTrue(ec().OK == temp.izbrisiVrednost(2, 2))
		temp.vpisiVrednost(1, 8, 1)
		self.assertTrue(ec().OK == temp.izbrisiVrednost(1, 8))
		temp.vpisiVrednost(2, 3, 1)
		self.assertTrue(ec().OK == temp.izbrisiVrednost(2, 3))

	def test_izbrisiVrednost3(self):
		temp = Sudoku(9, 9, 	[[v for v in range(1, 8+1)] + [0], \
								[v for v in range(4, 9+1)] + [v for v in range(1, 3+1)],\
								[v for v in range(7, 9+1)] + [v for v in range(1, 6+1)],\
								[0]*9,[0]*9,[0]*9,[0]*9,[0]*9,[0]*9])
		self.assertTrue(ec().OK == temp.izbrisiVrednost(8, 0))
		self.assertTrue(ec().FAIL_INITIAL == temp.izbrisiVrednost(1, 1))
		self.assertTrue(ec().FAIL_INITIAL == temp.izbrisiVrednost(2, 5))

	def test_izbrisiVrednost4(self):
		temp = Sudoku(9, 9)
		self.assertTrue(ec().FAIL_OUTOFBOUNDS == temp.izbrisiVrednost(0,-1))
		self.assertTrue(ec().FAIL_OUTOFBOUNDS == temp.izbrisiVrednost(-1, 9))
		self.assertTrue(ec().FAIL_OUTOFBOUNDS == temp.izbrisiVrednost(8, 9))


class TestPridobiPomoc(unittest.TestCase):
	
	def test_pridobiPomoc1(self):
		temp = Sudoku(9, 9)
		tempErr, seznam = temp.pridobiPomoc(0, 0, 0)
		self.assertTrue(ec().FAIL_EMPTY == tempErr)
		tempErr, seznam = temp.pridobiPomoc(5, 4, 1)
		self.assertTrue(ec().FAIL_EMPTY == tempErr)
		tempErr, seznam = temp.pridobiPomoc(8, 8, 2)
		self.assertTrue(ec().FAIL_EMPTY == tempErr)

	def test_pridobiPomoc2(self):
		temp = Sudoku(9, 9)
		tempErr, seznam = temp.pridobiPomoc(2, 3, -5)
		self.assertTrue(ec().FAIL_INVALIDARGUMENT == tempErr)
		tempErr, seznam = temp.pridobiPomoc(8, 4, 4)
		self.assertTrue(ec().FAIL_INVALIDARGUMENT == tempErr)
		tempErr, seznam = temp.pridobiPomoc(8, 4, 100)
		self.assertTrue(ec().FAIL_INVALIDARGUMENT == tempErr)

	def test_pridobiPomoc3(self):
		temp = Sudoku(9, 9)
		tempErr, seznam = temp.pridobiPomoc(-5, 6, 0)
		self.assertTrue(ec().FAIL_OUTOFBOUNDS == tempErr)
		tempErr, seznam = temp.pridobiPomoc(1, 9, 0)
		self.assertTrue(ec().FAIL_OUTOFBOUNDS == tempErr)

	def test_pridobiPomoc4(self):
		temp = Sudoku(9, 9)
		tempErr, seznam = temp.pridobiPomoc(-5, 6, 9)
		self.assertTrue(ec().FAIL_OUTOFBOUNDS == tempErr or ec().FAIL_INVALIDARGUMENT == tempErr)
		tempErr, seznam = temp.pridobiPomoc(1, 9, -2)
		self.assertTrue(ec().FAIL_OUTOFBOUNDS == tempErr or ec().FAIL_INVALIDARGUMENT == tempErr)

	def test_pridobiPomoc5(self):
		temp = Sudoku(9, 9, 	[[v for v in range(1, 8+1)] + [0], \
								[v for v in range(4, 9+1)] + [v for v in range(1, 3+1)],\
								[v for v in range(7, 9+1)] + [v for v in range(1, 6+1)],\
								[0]*9,[0]*9,[0]*9,[0]*9,[0]*9,[0]*9])

		tempErr, seznam = temp.pridobiPomoc(0, 0, 0)
		self.assertTrue(ec().OK == tempErr and len(seznam) == 3)
		self.assertTrue([0,0] in seznam and [1, 6] in seznam and [2, 3] in seznam)
		
	def test_pridobiPomoc6(self):
		temp = Sudoku(9, 9, 	[[v for v in range(1, 8+1)] + [0], \
								[v for v in range(4, 9+1)] + [v for v in range(1, 3+1)],\
								[v for v in range(7, 9+1)] + [v for v in range(1, 6+1)],\
								[0]*9,[0]*9,[0]*9,[0]*9,[0]*9,[0]*9])

		tempErr, seznam = temp.pridobiPomoc(0, 0, 1)
		self.assertTrue(ec().OK == tempErr and len(seznam) == 19)
		counter = 1
		for index in range(1,9):
			if ([index, 0] in seznam):
				counter = counter +1
			if ([0, index] in seznam):
				counter = counter + 1
		self.assertTrue(counter == 17)

	def test_pridobiPomoc7(self):
		temp = Sudoku(9, 9, 	[[v for v in range(1, 8+1)] + [0], \
								[v for v in range(4, 9+1)] + [v for v in range(1, 3+1)],\
								[v for v in range(7, 9+1)] + [v for v in range(1, 6+1)],\
								[0]*9,[0]*9,[0]*9,[0]*9,[0]*9,[0]*9])

		tempErr, seznam = temp.pridobiPomoc(0, 0, 2)
		self.assertTrue(ec().OK == tempErr and len(seznam) == 45)



if __name__ == '__main__':
	unittest.main(testRunner=HtmlTestRunner.HTMLTestRunner(output="reports/testingResults"))
	#unittest.main()
