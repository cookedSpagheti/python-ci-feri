# import unittest
import numpy as np
from errcodes import ErrCodes as ec
import copy
import random

def isValidSudoku(numpy_array, max_y, max_x, falty_list = []):
    returnValue = True
    isZeroPresent = False
    allValuesZero = True

    for sub_list in numpy_array:
        for item in sub_list:
            if item != 0:
                allValuesZero = False
                break
        if not allValuesZero:
            break

    if allValuesZero:
        return returnValue, True


    def isValueRepeating(items, current_index, max_index, checkingY = True):
        functionReturn = False
        values = []
        values_pos = []

        def addToFaltyList(element):
            temp = values_pos[ values.index(element) ]
            if temp not in falty_list:
                falty_list.append(temp)

            # for item in items:
        if not checkingY:
            for i in range(max_index):
                if items[i] in values:
                    falty_list.append([current_index, i])
                    addToFaltyList(items[i])
                    # return True
                    functionReturn = True
                    continue

                if items[i] == 0:
                    isZeroPresent = True
                    continue

                values.append(items[i])
                values_pos.append([current_index, i])
        else:
            for i in range(max_index):
                if items[i] in values:
                    falty_list.append([i, current_index])
                    addToFaltyList(items[i])
                    # return True
                    functionReturn = True
                    continue

                if items[i] == 0:
                    isZeroPresent = True
                    continue

                values.append(items[i])
                values_pos.append([current_index, i])

        return functionReturn



    # preveri vsako vrstico posebaj
    for i in range(max_y):
        if isValueRepeating( numpy_array[:, i], i, max_x):
            # return False, isZeroPresent
            returnValue = False


    # in preveri se vsak stolpec
    for i in range(max_x):
        if isValueRepeating( numpy_array[i, :], i, max_y, False):
            # return False, isZeroPresent
            returnValue = False

    # preveri del mreze
    def isSmallGridValid(row_start, col_start):
        functionReturn = True
        values = []

        for row in range(row_start, row_start + 3):
            for col in range(col_start, col_start + 3):
                item = numpy_array[row, col]

                if item in values:
                    falty_list.append([row, col])
                    # return False
                    functionReturn = False

                if item == 0:
                    isZeroPresent = True
                    continue

                values.append(item)


        return functionReturn



    for row in range(0, max_y, 3):
        for col in range(0, max_x, 3):
            if isSmallGridValid(row, col) == False:
                # return False, isZeroPresent
                returnValue = False


    # # nekje je napaka (odstrani iz seznama duplikate, ce so se pojavili)
    # if returnValue == False:
    #     # falty_list = np.unique(falty_list).tolist()
    #     falty_list = list(dict.fromkeys(falty_list))

    return returnValue, isZeroPresent

def generateRandomSoduku(m=3):
    import random
    n = m**2
    board = [[None for x in range(n)] for x in range(n)]

    def search(c=0):
        i, j = divmod(c, n)
        i0, j0 = i - i % m, j - j % m # Origin of mxm block
        numbers = list(range(1, n + 1))
        random.shuffle(numbers)

        for x in numbers:
            if (x not in board[i]                     # row
                and all(row[j] != x for row in board) # column
                and all(x not in row[j0:j0+m]         # block
                        for row in board[i0:i])):
                board[i][j] = x
                if c + 1 >= n**2 or search(c + 1):
                    return board
        else:
            # No number is valid in this cell: backtrack and try again.
            board[i][j] = None
            return None

    return search()


class Sudoku():  #unittest.TestCase
    # X = 0
    # Y = 0
    # original_mreza = np.zeros(1),
    # spreminjajoca_mreza = original_mreza
    # falty_items = []
    # temp_spreminjajoca_mreza = []

    def __init__(self, velikostY=9, velikostX=9, vrednosti=None, nakljucneVrednosti=None):
        self.X, self.Y = 0, 0
        self.original_mreza = np.zeros(1)
        self.spreminjajoca_mreza = np.zeros(1)
        self.falty_items = []

        # if velikostY == velikostX:
        #     raise Exception("FAIL_SIZE", "podani velikosti si nista enaki")

        if vrednosti is not None and nakljucneVrednosti is not None:
            raise Exception(ec().FAIL_INVALID_INITIALS, ec().getMsg(ec().FAIL_INVALID_INITIALS))

        # if velikostY % 3 != 0  or  velikostX % 3 != 0:
        if velikostY != 9 and velikostY != 16:
            raise Exception(ec().FAIL_SIZE, ec().getMsg(ec().FAIL_SIZE))
        else:
            if velikostX != 9 and velikostX != 16:
                raise Exception(ec().FAIL_SIZE, ec().getMsg(ec().FAIL_SIZE))

        if nakljucneVrednosti is not None:
            if nakljucneVrednosti > velikostX * velikostY:
                raise Exception(ec().FAIL_INVALID_INITIALS, ec().getMsg(ec().FAIL_INVALID_INITIALS))
            self.original_mreza = np.array(generateRandomSoduku())

            delete_values_num = velikostX * velikostY - nakljucneVrednosti
            while delete_values_num > 0:
                while True:
                    y = random.randint(0, velikostY-1)
                    x = random.randint(0, velikostX-1)

                    if self.original_mreza[y, x] != 0:
                        self.original_mreza[y, x] = copy.deepcopy(0)
                        delete_values_num -= 1
                        break

            self.spreminjajoca_mreza = copy.deepcopy(self.original_mreza)
            # return
        else:
            self.original_mreza = np.zeros([velikostY, velikostX], dtype=int)
            self.spreminjajoca_mreza = np.zeros([velikostY, velikostX], dtype=int)

            if vrednosti is not None:
                # if type(vrednosti) is not list:
                #     raise Exception(ec().FAIL_INVALID_INITIALS, ec().getMsg(ec().FAIL_INVALID_INITIALS))

                temp_mreza = []

                for skupek_vrednosti in vrednosti:
                    for vrednost in skupek_vrednosti:
                        if vrednost > 10 or -1 > vrednost:
                            raise Exception(ec().FAIL_INVALID_INITIALS, ec().getMsg(ec().FAIL_INVALID_INITIALS))
                        temp_mreza.append(vrednost)

                try:
                    self.original_mreza = np.array(temp_mreza)
                    self.original_mreza = self.original_mreza.reshape([velikostY, velikostX])
                    self.spreminjajoca_mreza = np.array(temp_mreza)
                    self.spreminjajoca_mreza = self.spreminjajoca_mreza.reshape([velikostY, velikostX])
                except:
                    # raise Exception(ec().FAIL_SIZE, ec().getMsg(ec().FAIL_SIZE))
                    raise Exception(ec().FAIL_INVALID_INITIALS, ec().getMsg(ec().FAIL_INVALID_INITIALS))

        self.X = copy.deepcopy(velikostX)
        self.Y = copy.deepcopy(velikostY)
        # self.temp_spreminjajoca_mreza = self.temp_spreminjajoca_mreza


    def dobiMrezo(self):
        if len(self.spreminjajoca_mreza) != 0:
            return ec().OK, copy.deepcopy(self.spreminjajoca_mreza), self.Y, self.X

        raise Exception(ec().FAIL_UNINITED, [self.spreminjajoca_mreza, self.Y, self.X])


    def preveriMrezo(self):
        sudokuValidaton, isStillEmpty = isValidSudoku(self.spreminjajoca_mreza, self.Y, self.X, self.falty_items)

        if sudokuValidaton:
            if isStillEmpty:
                return ec().VALID_NONFULL, self.falty_items

            return ec().VALID_FULL, self.falty_items

        return ec().FAULTY, self.falty_items


    def vpisiVrednost(self, pozicijaY, pozicijaX, vrednost):
        if vrednost < 1 or 9 < vrednost:
            return ec().FAIL_INVALID_INPUT

        # if (pozicijaY <= self.Y and pozicijaY > 0) == False and (pozicijaX <= self.X  and pozicijaY > 0) == False:
        #     return ec().FAIL_OUTOFBOUNDS
        if pozicijaY < self.Y and pozicijaY >= 0:
            if pozicijaX < self.X and pozicijaX >= 0:

                if self.original_mreza[pozicijaY, pozicijaX] == 0:
                    self.spreminjajoca_mreza[pozicijaY, pozicijaX] = vrednost
                    return ec().OK

                return ec().FAIL_INITIAL

        return ec().FAIL_OUTOFBOUNDS


    def izbrisiVrednost(self, pozicijaY, pozicijaX):
        if pozicijaY < self.Y and pozicijaY >= 0:
            if pozicijaX < self.X and pozicijaX >= 0:

                if self.original_mreza[pozicijaY, pozicijaX] == 0:
                    self.spreminjajoca_mreza[pozicijaY, pozicijaX] = 0
                    return ec().OK

                return ec().FAIL_INITIAL

        return ec().FAIL_OUTOFBOUNDS


    def pridobiPomoc(self, pozicijaY, pozicijaX, nivoPomoci):
        if pozicijaY < self.Y and pozicijaY >= 0:
            if pozicijaX < self.X and pozicijaX >= 0:

                if nivoPomoci < 0 or nivoPomoci > 2:
                    return ec().FAIL_INVALIDARGUMENT, []

                if np.array_equal(self.spreminjajoca_mreza, np.zeros([self.Y, self.X])):
                    return ec().FAIL_EMPTY, []

                if nivoPomoci == 0 or nivoPomoci == 1 or nivoPomoci == 2:
                    value = self.spreminjajoca_mreza[pozicijaY, pozicijaX]
                    result_array = []

                    for row in range(self.Y):
                        for col in range(self.X):
                            if value == self.spreminjajoca_mreza[row, col]:
                                result_array.append([row, col])

                    sameValuesCount = len(result_array)

                    if nivoPomoci == 1 or nivoPomoci == 2:
                        def saveRowColForGivenNumb(position):
                            current_y, current_x = position[0], position[1]

                            for y in range(self.Y):
                                if y != current_y:
                                    result_array.append([y, current_x])

                            for x in range(self.X):
                                if x != current_x:
                                    result_array.append([current_y, x])

                        saveRowColForGivenNumb([pozicijaY, pozicijaX])


                        if nivoPomoci == 2:
                            for i in range(sameValuesCount):
                                saveRowColForGivenNumb(result_array[i])

                    output = []
                    [output.append(x) for x in result_array if x not in output]
                    return ec().OK, output

                # return ec().FAIL_INVALIDARGUMENT, []

        return ec().FAIL_OUTOFBOUNDS, []